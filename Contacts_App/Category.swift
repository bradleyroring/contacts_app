//
//  Category.swift
//  Contacts_App
//
//  Created by Bradley Roring on 10/13/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import Foundation
import RealmSwift

class Category : Object {
    dynamic var fname = ""
    dynamic var lname = ""
    dynamic var phone = ""
    dynamic var email = ""
    
}
