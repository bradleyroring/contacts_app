//
//  SecondViewController.swift
//  Contacts_App
//
//  Created by Bradley Roring on 10/13/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class SecondViewController: UIViewController,UITextFieldDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate{
    
    var incomingContact: Category? = nil
    let realm = try! Realm()
    var emailReceipient = [String]()

    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var emailBox: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (incomingContact != nil) {
            try! realm.write {
                firstName.text = incomingContact?.fname
                lastName.text = incomingContact?.lname
                phoneNumber.text = incomingContact?.phone
                emailBox.text = incomingContact?.email
                emailReceipient.append(emailBox.text!)
            }
        }
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.phoneNumber.delegate = self
        self.emailBox.delegate = self
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        self.navigationItem.rightBarButtonItems = [saveBtn]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func buttonTapped (_ sender : UIButton) {
        print("save")
        if let existingContact = incomingContact {
            try! realm.write {
                existingContact.fname = firstName.text!
                existingContact.lname = lastName.text!
                existingContact.phone = phoneNumber.text!
                existingContact.email = emailBox.text!
                
            }
        }
        else {
            let newContact = Category()
            newContact.fname = firstName.text!
            newContact.lname = lastName.text!
            newContact.phone = phoneNumber.text!
            newContact.email = emailBox.text!
            
            try! realm.write {
                realm.add(newContact)
            }
        }
        navigationController?.popViewController(animated: true)
    }
    @IBAction func callButton(_ sender: Any) {
        if let url = URL(string: "tel://\(phoneNumber.text!)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func textButton(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.recipients = [phoneNumber.text!]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
   @IBAction func emailButton(_ sender: Any) {
        if MFMailComposeViewController.canSendMail()
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients(emailReceipient)
            
            self.present(composeVC, animated: true)
        }
    }
    //keep error away and handle screen actions
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
//hiding the keyboard on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
