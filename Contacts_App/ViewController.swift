//
//  ViewController.swift
//  Contacts_App
//
//  Created by Bradley Roring on 10/13/17.
//  Copyright © 2017 Bradley Roring. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

var contacts: Results<Category>!
let realm = try! Realm()

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var contactsTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactsTable.dataSource = self
        contactsTable.delegate = self
        contacts = realm.objects(Category.self)
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reload()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let contactRow = contacts[indexPath.row]
        cell.textLabel?.text = contactRow.fname + " " + contactRow.lname
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "addNew", sender: contacts[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addNew" {
            let destination = segue.destination as! SecondViewController
            destination.incomingContact = sender as? Category
        }
    }
    func reload() {
        contactsTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write{
                realm.delete(contacts[indexPath.row])
            }
            reload()
        }
        
    }
    
}

